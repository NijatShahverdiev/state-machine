package az.mia.statemachine.account;

import az.mia.statemachine.account.transition.Approve;
import az.mia.statemachine.account.transition.Notify;
import az.mia.statemachine.account.transition.Reject;
import az.mia.statemachine.account.transition.Submit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum AccountStatus {

    DRAFT(Submit.NAME),
    IN_REVIEW(Reject.NAME, Approve.NAME),
    APPROVE(Approve.NAME, Notify.NAME),
    NOTIFIED;

    private final List<String> allowedTransitions;

    AccountStatus(String ... names) {
        allowedTransitions = Arrays.stream(names)
                .collect(Collectors.toList());
    }

    public List<String> getAllowedTransitions(){
        return allowedTransitions;
    }
}
