package az.mia.statemachine.account;

import az.mia.statemachine.domain.Account;
import az.mia.statemachine.dto.AccountDto;
import az.mia.statemachine.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class TransitionService {

    private final AccountRepository accountRepository;

    private final List<Transition<AccountDto>> transitions;

    private final Map<String, Transition<AccountDto>> transitionMap = new HashMap<>();

    private final ModelMapper modelMapper;

    @PostConstruct
    public void init() {
        for (Transition<AccountDto> transition : transitions) {
            if (transitionMap.containsKey(transition.getName())) {
                throw new RuntimeException("Duplicated transition: " + transition.getName());
            } else {
                transitionMap.put(transition.getName(), transition);
            }
        }
    }

    public List<String> getAllowedTransitions(Long id) {
        Account account = getAccount(id);
        return account.getAccountStatus().getAllowedTransitions();
    }

    @Transactional
    public void transition(Long id, String transition) {
        checkTransition(transition);
        Account account = getAccount(id);
        checkIfTransitionAllowed(account, transition);
        Transition<AccountDto> accountTransition = transitionMap.get(transition);
        accountTransition.applyProcess(modelMapper.map(account, AccountDto.class));
        account.setAccountStatus(accountTransition.getTargetStatus());
        accountRepository.save(account);
    }

    private void checkTransition(String transition) {
        if (!transitionMap.containsKey(transition)) {
            throw new RuntimeException("Unknown transition: " + transition);
        }
    }

    private Account getAccount(Long id) {
        return accountRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Account not found with given id: " + id));
    }

    private void checkIfTransitionAllowed(Account account, String transition) {
        boolean contains = account.getAccountStatus().getAllowedTransitions()
                .contains(transition);
        if (!contains) {
            throw new RuntimeException("Transition " + transition + " is not allowed with id " + account.getId());
        }
        getAllowedTransitions(account.getId()).forEach(System.out::println);
    }
}
