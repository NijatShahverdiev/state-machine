package az.mia.statemachine.account;

public interface Transition<T> {

    String getName();

    AccountStatus getTargetStatus();

    void applyProcess(T account);
}
