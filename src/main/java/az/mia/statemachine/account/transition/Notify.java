package az.mia.statemachine.account.transition;

import az.mia.statemachine.account.AccountStatus;
import az.mia.statemachine.account.Transition;
import az.mia.statemachine.dto.AccountDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class Notify implements Transition<AccountDto> {

    public static final String NAME ="notify";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public AccountStatus getTargetStatus() {
        return AccountStatus.NOTIFIED;
    }

    @Override
    public void applyProcess(AccountDto account) {
        log.info("Transition into state {}", NAME);
    }
}
