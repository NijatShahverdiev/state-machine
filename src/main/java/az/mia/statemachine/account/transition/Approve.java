package az.mia.statemachine.account.transition;

import az.mia.statemachine.account.AccountStatus;
import az.mia.statemachine.account.Transition;
import az.mia.statemachine.dto.AccountDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class Approve implements Transition<AccountDto> {

    public static final String NAME ="approve";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public AccountStatus getTargetStatus() {
        return AccountStatus.APPROVE;
    }

    @Override
    public void applyProcess(AccountDto account) {
        log.info("Transition into state {}", NAME);
    }
}
