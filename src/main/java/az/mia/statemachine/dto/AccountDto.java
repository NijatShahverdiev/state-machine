package az.mia.statemachine.dto;

import az.mia.statemachine.account.AccountStatus;
import lombok.Data;

@Data
public class AccountDto {
    private Long id;

    private String name;

    private String iban;

    private AccountStatus accountStatus;
}
