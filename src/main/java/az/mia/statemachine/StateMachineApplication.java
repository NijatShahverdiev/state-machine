package az.mia.statemachine;

import az.mia.statemachine.account.AccountStatus;
import az.mia.statemachine.account.TransitionService;
import az.mia.statemachine.account.transition.Notify;
import az.mia.statemachine.account.transition.Submit;
import az.mia.statemachine.domain.Account;
import az.mia.statemachine.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
@RequiredArgsConstructor
public class StateMachineApplication implements CommandLineRunner {

    private final AccountRepository accountRepository;

    private final TransitionService transitionService;

    public static void main(String[] args) {
        SpringApplication.run(StateMachineApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        Account account = new Account();
        account.setIban("23472384");
        account.setName("Nicat");
        account.setAccountStatus(AccountStatus.DRAFT);

        accountRepository.save(account);
        transitionService.transition(account.getId(), Submit.NAME);
        transitionService.transition(account.getId(), Submit.NAME);
    }
}
