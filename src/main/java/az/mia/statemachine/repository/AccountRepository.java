package az.mia.statemachine.repository;

import az.mia.statemachine.domain.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, Long> {
}
